package com.kfwebstandard.servlets;

import com.kfwebstandard.beans.FishData;
import com.kfwebstandard.jdbcpersistence.FishDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;

/**
 * Servlet class that displays the contents of the Aquarium database as a simple
 * HTML table using JDBC
 *
 * @author Ken Fogel
 */
@WebServlet(name = "JDBCServlet", urlPatterns = {"/JDBCServlet"})
public class JDBCServlet extends HttpServlet {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(JDBCServlet.class);

    @Inject
    private FishDAO fishDAO;

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean success = true;
        List<FishData> fishies = null;

        try {
            fishies = fishDAO.findAll();
        } catch (SQLException ex) {
            Logger.getLogger(JDBCServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(
                    "<!doctype html>\n"
                    + "<html>\n"
                    + "<head>\n"
                    + "  <title>JDBC and Servlet</title>\n"
                    + "</head>\n");
            out.println("<p><TABLE RULES=all>");
            if (fishies != null) {
                for (FishData f : fishies) {
                    out.println("<tr>");
                    out.print("<td>"
                            + f.getId()
                            + "</td><td>"
                            + f.getCommonName()
                            + "</td><td>"
                            + f.getLatin()
                            + "</td><td>"
                            + f.getPh()
                            + "</td><td>"
                            + f.getKh()
                            + "</td><td>"
                            + f.getTemp()
                            + "</td><td>"
                            + f.getFishSize()
                            + "</td><td>"
                            + f.getSpeciesOrigin()
                            + "</td><td>"
                            + f.getTankSize()
                            + "</td><td>"
                            + f.getStocking()
                            + "</td><td>"
                            + f.getDiet()
                            + "</td>");
                    out.println("</tr>");
                }
            }
            out.println("</table></body></html>");
        }
    }
}
